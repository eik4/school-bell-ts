declare global {
  namespace Store {
    namespace Schedule {
      namespace Bell {
        namespace Daily {
          type IndexedTimePeriod = Index<TimePeriod>
        }
        type Daily = Hash<Daily.IndexedTimePeriod>
      }
      type Bell = Hash<Bell.Daily>

      type GetStageParams = {
        day: number,
        time: string
      }
    }
  }
}

export {}

