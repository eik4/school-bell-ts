declare global {
  type Hash<T> = {
    [key: string]: T
  }

  type Index<T> = {
    [index: number]: T
  }

  type TimePeriod = {
    start: string,
    finish: string
  }

  type StagedTimePeriod = {
    name: string,
    start: string,
    finish: string
  }
}

export {}