import moment from 'moment'

type ScheduleState = {
  bell: Store.Schedule.Bell,
  currentBellSchedule: string,
  humanizeMomentDay: Index<string>,
  stage: Hash<StagedTimePeriod[]>
}

const defaultState: ScheduleState = {
  bell: {
    'Стандартное': {
      'mon': {
        1: { start: '08:30', finish: '09:15' },
        2: { start: '09:35', finish: '10:20' },
        3: { start: '10:35', finish: '11:20' },
        4: { start: '11:35', finish: '12:20' },
        5: { start: '12:35', finish: '13:20' },
        6: { start: '13:35', finish: '14:20' }
      },
      'tue': {
        1: { start: '08:30', finish: '09:15' },
        2: { start: '09:35', finish: '10:20' },
        3: { start: '10:35', finish: '11:20' },
        4: { start: '11:35', finish: '12:20' },
        5: { start: '12:35', finish: '13:20' },
        6: { start: '13:35', finish: '14:20' }
      },
      'wed': {
        1: { start: '08:30', finish: '09:15' },
        2: { start: '09:35', finish: '10:20' },
        3: { start: '10:35', finish: '11:20' },
        4: { start: '11:35', finish: '12:20' },
        5: { start: '12:35', finish: '13:20' },
        6: { start: '13:35', finish: '14:20' }
      },
      'thu': {
        1: { start: '08:30', finish: '09:15' },
        2: { start: '09:35', finish: '10:20' },
        3: { start: '10:35', finish: '11:20' },
        4: { start: '11:35', finish: '12:20' },
        5: { start: '12:35', finish: '13:20' },
        6: { start: '13:35', finish: '14:20' }
      },
      'fri': {
        1: { start: '08:30', finish: '09:15' },
        2: { start: '09:35', finish: '10:20' },
        3: { start: '10:35', finish: '11:20' },
        4: { start: '11:35', finish: '12:20' },
        5: { start: '12:35', finish: '13:20' },
        6: { start: '13:35', finish: '14:20' }
      },
      'sat': {
      },
      'sun': {}
    }
  },
  currentBellSchedule: 'Стандартное',
  humanizeMomentDay: {
    0: 'sun',
    1: 'mon',
    2: 'tue',
    3: 'wed',
    4: 'thu',
    5: 'fri',
    6: 'sat'
  },
  stage: {}
}

export const state = () => ({
  ...defaultState
})

export const mutations = {
  // todo: VALIDATE_BELL_SCHEDULE
  PREPARE_STAGES(state: ScheduleState): void {
    const schedule: Store.Schedule.Bell.Daily = state.bell[state.currentBellSchedule];
    for (let dayKey in schedule) {
      state.stage[dayKey] = []
      const day: Index<TimePeriod> = schedule[dayKey];
      const timePeriodIndexes: string[] = Object.keys(day);
      if (timePeriodIndexes.length > 0) {
        state.stage[dayKey].push({
          name: 'before',
          start: '00:00',
          finish: day[parseInt(timePeriodIndexes[0])].start
        })
        timePeriodIndexes.forEach((timePeriodIndex: string, timePeriodIndexIndex: number) => {
          const timePeriod: TimePeriod = day[parseInt(timePeriodIndexes[timePeriodIndexIndex])]
          if (timePeriodIndexIndex < timePeriodIndexes.length - 1) {
            const nexTimePeriod: TimePeriod = day[parseInt(timePeriodIndexes[timePeriodIndexIndex + 1])]
            state.stage[dayKey].push({
              name: `Урок ${timePeriodIndex}`,
              start: timePeriod.start,
              finish: timePeriod.finish
            })
            state.stage[dayKey].push({
              name: `Перемена`,
              start: timePeriod.finish,
              finish: nexTimePeriod.start
            })
          }
          else {
            state.stage[dayKey].push({
              name: `Урок ${timePeriodIndex}`,
              start: timePeriod.start,
              finish: timePeriod.finish
            })
            state.stage[dayKey].push({
              name: `after`,
              start: timePeriod.finish,
              finish: '24:00'
            })
          }
        })
      }
    }
  }
}

export const actions = {}

export const getters = {
  getStage: (state: ScheduleState) => (params: Store.Schedule.GetStageParams): any => {
    if (state.humanizeMomentDay[params.day] === undefined) {
      console.error(`invalid day ${params.day}`)
      return {
        status: 'error'
      }
    }
    const stage = state.stage[state.humanizeMomentDay[params.day]]
    if (stage.length === 0) {
      return {
        status: 'no bells today'
      }
    }
    return {
      status: 'ok',
      bell: Object.values(stage).find((s: StagedTimePeriod, index: number) => {
        return params.time >= `${s.start}:00` && params.time < `${s.finish}:00`
      })
    }
  }
}