import moment from 'moment'

type ClockState = {
  started: boolean,
  intervalID: NodeJS.Timeout | null,
  intervalDelay: number,
  status: string | null,
  mainTimeString: string | null,
  footerTimeString: string | null
}

const defaultState: ClockState = {
  started: false,
  intervalID: null,
  intervalDelay: 100,
  status: null,
  mainTimeString: null,
  footerTimeString: null
}

export const state = () => ({
  ...defaultState
})

export const mutations = {
  START(state: ClockState): void {
    if (!state.started) {
      state.started = true;
      this.commit('clock/TICK');
      state.intervalID = setInterval(() => {
        this.commit('clock/TICK');
      }, state.intervalDelay);
      
    }
  },

  STOP(state: ClockState): void {
    clearInterval(state.intervalID);
    state.intervalID = null;
    state.started = false;
  },

  TICK(state: ClockState): void {
    const now = moment()
    state.mainTimeString = now.format('HH:mm:ss');
    const stage = this.getters['schedule/getStage']({
      day: now.day(),
      time: now.format('HH:mm:ss')
    })
    if (stage.status === 'ok') {
      if (stage.bell.name === 'before') {
        const finish = moment(stage.bell.finish, 'HH:mm');
        finish.subtract(<any>now);
        finish.subtract(finish.utcOffset(), 'minutes');
        state.status = `Занятия начнутся через`;
        state.mainTimeString = finish.format('HH:mm:ss');
        state.footerTimeString = now.format('HH:mm:ss');
      }
      else if (stage.bell.name === 'after') {
        state.status = `Занятий больше нет`;
        state.mainTimeString = now.format('HH:mm:ss');
        state.footerTimeString = null;
      }
      else {
        const finish = moment(stage.bell.finish, 'HH:mm');
        finish.subtract(<any>now);
        finish.subtract(finish.utcOffset(), 'minutes');
        state.status = `${stage.bell.name} закончится через`;
        state.mainTimeString = finish.format('HH:mm:ss');
        state.footerTimeString = now.format('HH:mm:ss');
      }
    }
    else if (stage.status === 'no bells today') {
      state.status = 'Сегодня занятий нет';
      state.mainTimeString = now.format('HH:mm:ss');
      state.footerTimeString = null;
    }
    else {
      state.status = null;
      state.mainTimeString = now.format('HH:mm:ss');
      state.footerTimeString = null;
    }
  },

  CLEAR_STATE(state: ClockState): void {
    Object.keys(defaultState).forEach((key: string) => {
      (<any>state)[key] = (<any>defaultState)[key]
    })
  }
}

export const actions = {}